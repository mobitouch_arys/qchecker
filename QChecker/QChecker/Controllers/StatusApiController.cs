﻿using Microsoft.AspNet.SignalR;
using QChecker.Hubs;
using System;
using System.IO;
using System.Web.Http;

namespace QChecker.Controllers
{
    [RoutePrefix("api")]
    public class StatusApiController : ApiController
    {
        [HttpGet]
        [Route("status")]
        public IHttpActionResult GetStatus()
        {
            var status = 1;
            string appDataFolder = AppDomain.CurrentDomain.GetData("DataDirectory").ToString();

            try
            {
                var ds = Directory.GetAccessControl(appDataFolder);
                string fileName = "status.json";
                string fullPath = Path.Combine(appDataFolder, fileName);

                if (File.Exists(fullPath))
                {
                    status = Convert.ToInt32(File.ReadAllText(fullPath));
                }
            }
            catch (Exception ex)
            {
                if (ex is UnauthorizedAccessException)
                {
                    File.WriteAllText(@"C:\\Log\log.txt", "No access to: " + ex.Message);
                }

                File.WriteAllText(@"C:\\Log\log.txt", ex.GetType().Name + ex.Message + ex.StackTrace);
            }

            return Ok(status);
        }

        [HttpPut]
        [Route("status/{value}")]
        public IHttpActionResult ChangeStatus(int value)
        {
            var hub = GlobalHost.ConnectionManager.GetHubContext<QueueNotificationHub>();
            hub.Clients.All.newMessage(value);

            try
            {
                string appDataFolder = AppDomain.CurrentDomain.GetData("DataDirectory").ToString();
                string fileName = "status.json";
                string fullPath = Path.Combine(appDataFolder, fileName);
                File.WriteAllText(fullPath, value.ToString());
            }
            catch (Exception ex)
            {
                File.WriteAllText("C://log.txt", ex.Message);
            }

            return Ok();
        }
    }
}
