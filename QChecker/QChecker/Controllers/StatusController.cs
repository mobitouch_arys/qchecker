﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QChecker.Controllers
{
    public class StatusController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Check()
        {
            var status = 1;
            string appDataFolder = AppDomain.CurrentDomain.GetData("DataDirectory").ToString();
            string fileName = "status.json";
            string fullPath = Path.Combine(appDataFolder, fileName);

            if (System.IO.File.Exists(fullPath))
            {
                status = Convert.ToInt32(System.IO.File.ReadAllText(fullPath));
            }

            ViewBag.Mode = status;

            return View();
        }

        [Authorize(Users = "admin@deloittece.com, Nero@deloittece.com, mode1@deloittece.com, mode2@deloittece.com, mode3@deloittece.com")]
        public ActionResult Manage()
        {
            return View();
        }

        public ActionResult Cafeteria()
        {
            return View("Index");
        }
    }
}