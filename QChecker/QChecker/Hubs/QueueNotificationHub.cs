﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.IO;

namespace QChecker.Hubs
{
    [HubName("queueNotificationHub")]
    public class QueueNotificationHub : Hub
    {
        public void SendMessage(int message)
        {
            var fullPath = string.Empty;

            Clients.All.newMessage(message);

            try
            {
                string appDataFolder = AppDomain.CurrentDomain.GetData("DataDirectory").ToString();
                string fileName = "status.json";
                fullPath = Path.Combine(appDataFolder, fileName);
                File.WriteAllText(fullPath, message.ToString());
                File.WriteAllText(@"C:\\Log\log.txt", fullPath);
            }
            catch (Exception ex)
            {
                File.WriteAllText(@"C:\\Log\log.txt", ex.Message);
            }
        }
    }
}