﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(QChecker.Startup))]
namespace QChecker
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
